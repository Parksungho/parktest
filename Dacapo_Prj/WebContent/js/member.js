function checkID() {
	if (document.frm.id.value == "") {
		alert('아이디를 입력하여 주십시오.');
		document.frm.id.focus();
		return;
	}
	var url = "member?menu=idcheck&id=" + document.frm.id.value;
	window
			.open(url, "_blank_1",
					"toolbar=no, menubar=no, scrollbars=yes, resizable=no, width=450, height=200");
}

function selectMail() {
	if (document.frm.selectmail.value == "직접입력") {
		document.frm.bemail.value = "";
	} else {
		document.frm.bemail.value = document.frm.selectmail.value;
	}
}

function checkForm() {
	if (document.frm.id.value.length == 0) {
		alert("아이디를 써주세요");
		frm.id.focus();
		return false;
	}

	if (document.frm.id.value.length < 4) {
		alert("아이디는 4글자이상이어야 합니다.");
		frm.id.focus();
		return false;
	}

	if (document.frm.idCheck.value.length == 0) {
		alert("중복 체크를 하지 않았습니다.");
		frm.id.focus();
		return false;
	}

	if (document.frm.pass.value == "") {
		alert("암호는 반드시 입력해야 합니다.");
		frm.pass.focus();
		return false;
	}

	if (document.frm.pass.value != document.frm.passCheck.value) {
		alert("암호가 일치하지 않습니다.");
		frm.pass.focus();
		return false;
	}

	if (document.frm.name.value.length == 0) {
		alert("이름을  써주세요.");
		frm.name.focus();
		return false;
	}

	return true;
}

function checkUpdateForm(){
	
	if(document.frm.pass.value.length == 0){
		alert("비밀번호를 입력해주세요");
		frm.pass.focus();
		return false;
	}
	
	if(document.frm.pass.value != document.frm.passCheck.value){
		alert("비밀번호를 확인해주세요");
		frm.passCheck.focus();
		return false;
	}

	
	return true;
	
}

function confirmDeleteMember(){
	if(document.frm.pass.value.length == 0){
		alert("비밀번호를 입력해주세요");
		frm.pass.focus();
		return false;
	}
	
	if(document.frm.pass.value != document.frm.passCheck.value){
		alert("비밀번호를 확인해주세요");
		frm.passCheck.focus();
		return false;
	}
	
	if(document.frm.pass.value != document.frm.originpass.value){
		alert("비밀번호가 틀렸습니다.");
		frm.pass.focus();
		return false;
		
	}
	
	return confirm("정말로 탈퇴하시겠습니까?");
	
}