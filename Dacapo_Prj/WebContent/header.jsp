<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset=UTF-8>
<title>쉼:표(,)</title>
<link rel="stylesheet" href="/Dacapo_Prj/CSS/default.css">
</head>
<body>
	<div id="wrap">
		<header>
			<div id="logo">
				<a href="/Dacapo_Prj"><img src="/Dacapo_Prj/images/logo.png"></a>
			</div>

			<div id="user_menu">
				<c:choose>
					<c:when test="${loginUser == null}">
						<form action="/Dacapo_Prj/service/member?menu=login" method="post">
						
							<a href="#">아이디/비밀번호 찾기</a> <a
								href="/Dacapo_Prj/service/member?menu=readyJoinForm&type=join">회원가입</a><br>
							ID<input type="text" name="id"> PW<input type="password"
								name="pass"> <input type="submit" value="로그인"><br>
								${msg}
								
						</form>
					</c:when>
					<c:otherwise>
						${loginUser.name}님 환영합니다.
							<c:if test="${loginUser.id == 'dacapo'}">
								<input type="button" value="관리페이지"
									onclick="location.href='/Dacapo_Prj/service/member?menu=getAll'">
							</c:if>
						<input type="button" value="회원정보수정"
							onclick="location.href='/Dacapo_Prj/service/member?menu=readyJoinForm&type=update'">
						<input type="button" value="로그아웃"
							onclick="location.href='/Dacapo_Prj/service/member?menu=logout'">
					</c:otherwise>
				</c:choose>
			</div>
	


			<nav id="top_menu">
				<ul>
					<li><a href="/Dacapo_Prj/">HOME</a></li>
					<li><a href="#">대기정보</a></li>
					<li><a href="/Dacapo_Prj/service/board?menu=getAllFestival">Festival</a></li>
					<li><a href="/Dacapo_Prj/service/board?menu=getAllRiding">Riding 게시판</a></li>
					<li><a href="/Dacapo_Prj/service/board?menu=getAll">QnA</a></li>
					
				</ul>
			</nav>
			<div id="clear"></div>

