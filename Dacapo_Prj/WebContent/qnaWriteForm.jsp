<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<link rel="stylesheet" href="/Dacapo_Prj/CSS/board.jsp">

<div id="writeForm">
	<form action=service/board?menu=write method="post">
		<table>
			<tr>
				<td>작성자</td>
				<td><input type="hidden" name="id" value="${loginUser.id}">${loginUser.id}</td>
			</tr>

			<tr>
				<td>비밀번호</td>
				<td><input type="password" name="pass"></td>
			</tr>

			<tr>
				<td>제목</td>
				<td><input type="text" name="title"></td>
			</tr>

			<tr>
				<td>내용</td>
				<td><textarea rows="10" cols="20" name="content"></textarea></td>
			</tr>

			<tr>
				<td colspan=2><input type="submit" value="글쓰기"><input
					type="button" value="취소"
					onclick="location.href='service/board?menu=getAll'">
		</table>
	</form>
</div>
</body>
</html>