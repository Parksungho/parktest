<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../header.jsp"%>
<link rel="stylesheet" href="/Dacapo_Prj/CSS/board.css">


<div id="article">
	<table>
		<tr>
			<th class="writer">작성자</th>
			<td>${qna.id}</td>
		</tr>

		<tr>
			<th class="title">제목</th>
			<td>${qna.title}</td>
		</tr>

		<tr>
			<th class="content">내용</th>
			<td>${qna.content}</td>
		</tr>
	</table>
</div>


<div id="articleSelecter">
	<table>
		<tr><form>
			<td><c:if test="${loginUser.id == qna.id}">
					
						<input type="hidden" name="bnum" value="${qna.num}">
						<button formaction="board?menu=update" formmethod="post">수정하기</button>
						<button formaction="board?menu=delete" formmethod="post">삭제하기</button>
					
				</c:if> <input type="button" value="목록보기"
				onclick="location.href='board?menu=getAll'"></td>
				</form>
		</tr>
	</table>
</div>

<div id="reply">
	<table>
		<c:forEach items="${reply}" var="reply">
			<form
				action="board?menu=deleteReply&bnum=${qna.num}&rnum=${reply.rnum}"
				method="post">
				<tr>
					<td>${reply.id}</td>
					<td>${reply.content} <c:if test="${reply.id == loginUser.id}">
							<input type="submit" value="삭제">
						</c:if>
					</td>

				</tr>

			</form>
		</c:forEach>

		<c:if test="${loginUser.id == 'dacapo' || loginUser.id=='qna.id'}">
			<form action="board?menu=writeReply" method="post">
				<tr>
					<td colspan=2><input type="hidden" name="id"
						value="${loginUser.id}"> <input type="hidden" name="bnum"
						value="${qna.num}"> <textarea name="content" rows="3"
							cols="70"></textarea></td>
				</tr>

				<tr>
					<td colspan=2><input type="submit" id="reply_btn" value="댓글쓰기"></td>
				</tr>
			</form>
		</c:if>

	</table>
</div>



</body>
</html>