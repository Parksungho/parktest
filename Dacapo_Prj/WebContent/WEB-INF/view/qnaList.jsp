<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../../header.jsp"%>
<link rel="stylesheet" href="/Dacapo_Prj/CSS/board.css">
<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/media/images/favicon.ico"/>
<link rel="/Dacapo_Prj/media/css/demo_table_jui.css">
<style type="text/css" title="currentStyle">
	@import "/Dacapo_Prj/jquery.dataTables.css";
</style>
<script src="/Dacapo_Prj/jquery.js" type="text/javascript"></script>
<script src="/Dacapo_Prj/jquery.dataTables.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
	$('#qnaList').dataTable();
});
</script>
<div id="boardList">
	<table class="display" id="qnaList">
	<thead>
		<tr>
			<th class="num">번호</th>
			<th class="title">제목</th>
			<th class="writer">작성자</th>
		</tr>
	</thead>
	
	<tbody>
		<c:forEach items="${qnaList}" var="list">
			<tr>
				<td>${list.num}</td>
				<td><a href="board?menu=get&bnum=${list.num}">${list.title}</a></td>
				<td>${list.id}</td>
			</tr>
		</c:forEach>
	</tbody>
	</table>
</div>

<c:if test="${loginUser.id !=null }">
	<div id="boardSelecter">

		<input id="write_Btn" type="button" value="글쓰기"
			onclick="location.href='../qnaWriteForm.jsp'">

	</div>
</c:if>

</body>
</html>