<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	전체회원정보
	<table>
		<tr>
			<th>아이디</th>
			<th>이름</th>
			<th>핸드폰</th>
			<th>이메일</th>
			<th>강제탈퇴</th>
		</tr>
		<c:forEach items="${memberList}" var="list">
			<form action="../service/member?menu=delete" method="post">
			<tr>
				<td><input type="hidden" name="id" value="${list.id}">${list.id}</td>
				<td>${list.name}</td>
				<td>${list.phone}</td>
				<td>${list.email}</td>
				<td><input type="submit" value="강퇴"></td>
			<tr>
			</form>
		</c:forEach>
	</table>

<input type="button" value="축제정보업데이트" onclick="location.href='/Dacapo_Prj/service/parse?menu=parseFestival'">

</body>
</html>