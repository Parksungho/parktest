<%@ page language="java" contentType="text/json; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

[
	<c:forEach items="${parkList}" var="list" varStatus="status">
		{ "p_name":"${list}" }
		<c:if test="${!status.last}">
		,
		</c:if>
	</c:forEach>

]