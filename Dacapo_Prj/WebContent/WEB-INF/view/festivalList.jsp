<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../../header.jsp"%>
<link rel="stylesheet" href="/Dacapo_Prj/CSS/board.css">



<c:forEach items="${fList}" var="list">
	<div id="boardList">
		<table id="festivalTable">
			<tr>
				<td class="title1">행사명</td>
				<td>${list.title}</td>
			</tr>
			<tr>
				<td class="title1">장소</td>
				<td>${list.place}</td>
			</tr>
			<tr>
				<td class="title1">기간</td>
				<td>${list.sData} ~ ${list.eData}</td>
			</tr>
			<tr>
				<td class="title1">담당처</td>
				<td>${list.managerID}</td>
			</tr>
			<tr>
				<td class="title1">비고</td><td>${list.content}</td>
			</tr>
		</table>
	</div>
	
	<div id="clear">
		<br><Br><br>
	</div>
</c:forEach>



</body>
</html>