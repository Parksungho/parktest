<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="../../header.jsp"%>
<link rel="stylesheet" href="/Dacapo_Prj/CSS/board.css">

<script type="text/javascript" src="../js/member.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#myParkGu").change(function(){
		var spot = $("#myParkGu").val();
		console.log(spot);
	
		$.ajax({
			"url":"member",
			"data":"menu=getParkList&type=ajax&spot="+spot,
			"type":"post",
			"dataType":"json",
			"success":function(json){
				console.log(json);
				$("#myPark").empty()
				$.each(json,function(){
					$("#myPark").append("<option>"+this.p_name+"</option>")
				})
				
			}
			
		})
	})
})
</script>

<div id="login">
	<form name="frm" action="member?menu=join" method="post">

		<table>
			<tr>
				<td>아이디</td><td><input type="text" name="id"> <input
					type="hidden" name="idCheck"> <input type="button"
					value="중복확인" onclick="checkID()"></td>
			</tr>

			<tr>
				<td>비밀번호</td><td><input type="password" name="pass"></td>
			</tr>

			<tr>
				<td>비밀번호확인</td><td><input type="password" name="passCheck"></td>
			</tr>

			<tr>
				<td>이름</td><td><input type="text" name="name"></td>
			</tr>

			<tr>
				<td>주민번호</td><td><input type="text" name="jumin1">-<input
					type="password" name="jumin2"></td>
			</tr>

			<tr>
				<td>핸드폰번호</td><td><input type="text" name="phone1" size="4">-<input
					type="text" name="phone2" size="4">-<input type="text" name="phone3" size="4"></td>
			</tr>

			<tr>
				<td>이메일</td><td><input type="text" name="femail">@ <input
					type="text" name="bemail"> <select name="selectmail"
					onchange="selectMail()">
						<option>직접입력
						<option>naver.com
						<option>hanmail.net
						<option>gmail.com
				</select></td>
			</tr>
			
			<tr>
				<td>관심지역</td><td>
				<select id="mySpot" name="mySpot">
					<c:forEach items="${spotList}" var="list">
						<option>${list}
					</c:forEach>
				</select>
			</tr>

			<tr>
			<td>관심공원</td><Td>
				<select id="myParkGu" name="myParkGu">
					<c:forEach items="${spotList}" var="list">
						<option>${list}
					</c:forEach>
				</select>
				<select id="myPark" name="myPark">
				
				</select>
				</td>
			</tr>
			
			<tr>

				<td colspan=2><input type="submit" value="가입하기"
					onclick="return checkForm()"> <input type="button"
					value="취소" onclick="location.href='../index.jsp'"></td>
			</tr>
		</table>
	</form>
</div>
</body>
</html>