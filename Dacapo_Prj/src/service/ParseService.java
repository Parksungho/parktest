package service;


public class ParseService 
{
	private IParseService service;
	
	public void setService(IParseService service) {
		this.service = service;
	}
	
	
	public void parseAirXml()
	{
		service.parseAirXml();
	}
	public void parseFestivalXml()
	{
		service.ParseFestivalXml();
	}
	public void RemoveAirInfo(){
		service.RemoveAirInfo();
	}
	public void RemoveFestivalInfo(){
		service.RemoveFestivalRoadInfo();
	}
}
