package service;

import java.util.List;

import vo.MemberVO;

public interface IMemberService {
	
	void insertMember(MemberVO mvo);
	void deleteMember(MemberVO mvo);
	void updateMember(MemberVO mvo);
	MemberVO getMember(String id);
	List<MemberVO> getAllMembers();
	List<String> getSpots();
	List<String> getParks(String spot);
}
