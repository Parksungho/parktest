package service;

import java.util.List;

import vo.FestivalVO;
import dao.FestivalDAO;

public class FestivalServiceImpl implements IFestivalService 
{
	private FestivalDAO fdao;
	

	public void setFdao(FestivalDAO fdao) {
		this.fdao = fdao;
	}


	public List<FestivalVO> getAll() 
	{
		return fdao.getAll();
	}
}
