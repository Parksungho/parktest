package service;

import java.util.List;

import vo.BoardVO;
import vo.ReplyVO;

public class BoardService {

	private IBoardService service;

	public void setService(IBoardService service) {
		this.service = service;
	}
	
	public void insertArticle(BoardVO bvo){
		service.insert(bvo);
	}
	
	public void deleteArticle(int num){
		service.delete(num);
	}
	
	public void updateArticle(BoardVO bvo){
		service.update(bvo);
	}
	
	public BoardVO getArticle(int num){
		return service.get(num);			
	}
	
	public List<BoardVO> getAllArticle(){
		return service.getAll();
	}
	
	public void insertReply(ReplyVO rvo){
		service.insertReply(rvo);
	}
	
	public void deleteReply(int rnum){
		service.deleteReply(rnum);
	}
	
	public void updateReply(ReplyVO rvo){
		service.updateReply(rvo);
	}
	
	public ReplyVO getReply(int rnum){
		return service.getReply(rnum);
	}
	
	public List<ReplyVO> getAllReply(int bnum){
		return service.getAllReply(bnum);
	}
	
	public void deleteAllReply(int bnum){
		service.deleteAllReply(bnum);
	}
	
}

