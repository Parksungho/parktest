package service;

import java.util.List;

import vo.BoardVO;
import vo.ReplyVO;

interface IBoardService {
	
	void insert(BoardVO bvo);
	void update(BoardVO bvo);
	void delete(int num);
	BoardVO get(int num);
	List<BoardVO> getAll();
	
	void insertReply(ReplyVO rvo);
	void updateReply(ReplyVO rvo);
	void deleteReply(int rnum);
	ReplyVO getReply(int rnum);
	List<ReplyVO> getAllReply(int bnum);
	void deleteAllReply(int bnum);
}
