package service;

import java.util.List;

import vo.RidingBoardVO;
import vo.RidingListVO;
import vo.RidingReplyVO;

public class RidingService 
{
	private IRidingService service;
	
	public void setService(IRidingService service)
	{
		this.service=service;
	}
	public void RidingBoardInsert(RidingBoardVO rVo) 
	{
		service.RidingBoardInsert(rVo);
	}
	public void RidingBoardDelete(RidingBoardVO rVo) 
	{
		service.RidingBoardDelete(rVo);
	}
	public RidingBoardVO RidingBoardGet(String id) 
	{
		return service.RidingBoardGet(id);
	}
	public List<RidingBoardVO> RidingBoardGetAll()
	{
		return service.RidingBoardGetAll();
	}
	public void RidingReplyInsert(RidingReplyVO rVo) 
	{
		service.RidingReplyInsert(rVo);
	}
	public void RidingReplyDelete(RidingReplyVO rVo)
	{
		service.RidingReplyDelete(rVo);
	}
	public void RidingReplyUpdate(RidingReplyVO rVo) 
	{
		service.RidingReplyUpdate(rVo);
	}
	public RidingReplyVO RidingReplyGet(String id)
	{
		return service.RidingReplyGet(id);
	}
	public List<RidingReplyVO> RidingReplyGetAll() 
	{
		return service.RidingReplyGetAll();
	}
	public void RidingInsert(RidingListVO rVo)
	{
		service.RidingInsert(rVo);
	}
	public void RidingDelete(RidingListVO rVo)
	{
		service.RidingDelete(rVo);
	}
	public List<RidingListVO> RidingGetAll() 
	{
		return service.RidingGetAll();
	}
}
