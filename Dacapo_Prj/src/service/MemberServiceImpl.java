package service;

import java.util.List;

import dao.MemberDAO;

import vo.MemberVO;

public class MemberServiceImpl implements IMemberService{

	private MemberDAO mdao;
	
	public void setMdao(MemberDAO mdao) {
		this.mdao = mdao;
	}

	@Override
	public void insertMember(MemberVO mvo) {
		mdao.insert(mvo);
		
	}

	@Override
	public void deleteMember(MemberVO mvo) {
		mdao.delete(mvo);
		
	}

	@Override
	public void updateMember(MemberVO mvo) {
		mdao.update(mvo);
		
	}

	@Override
	public MemberVO getMember(String id) {
		// TODO Auto-generated method stub
		return mdao.get(id);
	}

	@Override
	public List<MemberVO> getAllMembers() {
		// TODO Auto-generated method stub
		return mdao.getAll();
	}

	@Override
	public List<String> getSpots() {
		// TODO Auto-generated method stub
		return mdao.getSpots();
	}

	@Override
	public List<String> getParks(String spot) {
		// TODO Auto-generated method stub
		return mdao.getParks(spot);
		
	}
	

}
