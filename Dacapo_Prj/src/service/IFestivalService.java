package service;

import java.util.List;

import vo.FestivalVO;

public interface IFestivalService 
{
	public List<FestivalVO> getAll();
}
