package vo;

public class ParkVO 
{
	private String parkName;
	private String parkContent;
	private String parkAddress;
	private String regionName;
	private String parkManager;
	private String parkIMG;
	private String ManagerPhone;
	private double longitude;
	private double latitude;
	
	public String getParkName() {
		return parkName;
	}
	public void setParkName(String parkName) {
		this.parkName = parkName;
	}
	public String getParkContent() {
		return parkContent;
	}
	public void setParkContent(String parkContent) {
		this.parkContent = parkContent;
	}
	public String getParkAddress() {
		return parkAddress;
	}
	public void setParkAddress(String parkAddress) {
		this.parkAddress = parkAddress;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getParkManager() {
		return parkManager;
	}
	public void setParkManager(String parkManager) {
		this.parkManager = parkManager;
	}
	public String getParkIMG() {
		return parkIMG;
	}
	public void setParkIMG(String parkIMG) {
		this.parkIMG = parkIMG;
	}
	public String getManagerPhone() {
		return ManagerPhone;
	}
	public void setManagerPhone(String managerPhone) {
		ManagerPhone = managerPhone;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	
}
