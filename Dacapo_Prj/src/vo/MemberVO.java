package vo;

//회원의 정보를 담는 Value Object
public class MemberVO {
	
	private String id;
	private String pass;
	private String name;
	private String pnum;
	private String email;
	private String phone;
	private String mypark;
	private String myspot;
	
	
	
	//변수 접근을 위한 getter,setter
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPnum() {
		return pnum;
	}
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMypark() {
		return mypark;
	}
	public void setMypark(String mypark) {
		this.mypark = mypark;
	}
	public String getMyspot() {
		return myspot;
	}
	public void setMyspot(String myspot) {
		this.myspot = myspot;
	}
	
	
	
	
	
	
	
	
}
