package vo;

public class RegionVO 
{
	private String regionName;
	private double longitude;
	private double latitude;
	private int regionSize;
	
	public String getRegionName() 
	{
		return regionName;
	}
	public void setRegionName(String regionName)
	{
		this.regionName = regionName;
	}
	public double getLongitude()
	{
		return longitude;
	}
	public void setLongitude(double longitude) 
	{
		this.longitude = longitude;
	}
	public double getLatitude()
	{
		return latitude;
	}
	public void setLatitude(double latitude) 
	{
		this.latitude = latitude;
	}
	public int getRegionSize() 
	{
		return regionSize;
	}
	public void setRegionSize(int regionSize)
	{
		this.regionSize = regionSize;
	}
	
	
}
