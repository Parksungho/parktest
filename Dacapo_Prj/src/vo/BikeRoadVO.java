package vo;

public class BikeRoadVO 
{
	private String bikeRoadID;
	private String bikeClass;
	private String bikeRoadAddress;
	private String regionName;
	private double latitude;
	private double longitude;
	
	public String getBikeRoadID() 
	{
		return bikeRoadID;
	}
	public void setBikeRoadID(String bikeRoadID) 
	{
		this.bikeRoadID = bikeRoadID;
	}
	public String getBikeRoadAddress() 
	{
		return bikeRoadAddress;
	}
	public void setBikeRoadAddress(String bikeRoadAddress) 
	{
		this.bikeRoadAddress = bikeRoadAddress;
	}
	public String getRegionName()
	{
		return regionName;
	}
	public void setRegionName(String regionName)
	{
		this.regionName = regionName;
	}
	public double getLatitude() 
	{
		return latitude;
	}
	public void setLatitude(double latitude) 
	{
		this.latitude = latitude;
	}
	public double getLongitude() 
	{
		return longitude;
	}
	public void setLongitude(double longitude) 
	{
		this.longitude = longitude;
	}
	public String getBikeClass() 
	{
		return bikeClass;
	}
	public void setBikeClass(String bikeClass) 
	{
		this.bikeClass = bikeClass;
	}
}
