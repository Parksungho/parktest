package controller;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.mvc.AbstractController;

import service.MemberService;
import vo.MemberVO;

//회원관련정보와 서비스를 처리하는 컨트롤러
public class MemberController extends AbstractController {

	private MemberService service;

	public void setService(MemberService service) {
		this.service = service;
	}

	
	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//쿼리스트링으로 날아온 menu로 실행할 서비스를 선택
		String menu = request.getParameter("menu").trim();
		
		
		//스위치문에서 사용할 목적으로 초기화
		HttpSession session = request.getSession();
		MemberVO mvo = new MemberVO();
		List<String> slist = new ArrayList<String>();

		
		switch (menu) {
		
		case "readyJoinForm":
			slist = service.getSpots();
			if(request.getParameter("type").trim().equals("update"))
				return new ModelAndView("updateMember","spotList",slist);
			return new ModelAndView("join","spotList",slist);
			
		case "getParkList":
			String spot = request.getParameter("spot").trim();
			System.out.println(spot);
			slist = service.getParks(spot);
			System.out.println(slist);
			String type = request.getParameter("type").trim();
			if(type == null)
				return new ModelAndView("","parkList",slist);
			else
				return new ModelAndView("parkList","parkList",slist);
				

		//회원 가입시 아이디 중복 체크
		case "idcheck":
			String result = null;
			//입력된 아이디 값을 인자로 하여 서비스로직에서 해당 아이디의 정보가 있는지 확인
			mvo = (MemberVO)service.getMember(request.getParameter("id"));

			//리턴된 인자의 유무에 따라 아이디 존재유무를 파악하여 사용자에게 메세지를 띄우기 위해 셋팅
			if (mvo != null) {
				result = request.getParameter("id") + " 는 사용 할 수 없는 아이디 입니다.";
			} else {
				result = request.getParameter("id") + " 는 사용가능한 아이디 입니다.";
			}
			//전달할 결과들이 각기 다른 타입의 객체들이므로 hashmap 생성
			HashMap<String, Object> data = new HashMap<String, Object>();
			
			//아이디존재유무를 알려주는 메세지 싣기
			data.put("result", result);
			//리턴된 객체의 유무를 통하여 '사용'버튼 만들기 위해 리턴객체주소 싣기
			data.put("vo", mvo);
			//가입폼의 hidden타입으로 있는 중복체크여부확인을 위해 존재하는 idcheck에 값을 넣어주기 위해 싣기
			data.put("id", request.getParameter("id"));

			// 뷰 리졸버에서 idcheck.jsp를 찾아 data와 함께 이동시켜줌
			return new ModelAndView("idcheck", "data", data);

		//회원가입
		case "join":
			
			//MemberVO객체에 회원가입폼에 입력한 값들을 채워 넣는다.
			mvo.setId(request.getParameter("id"));
			mvo.setPass(request.getParameter("pass"));
			mvo.setName(request.getParameter("name"));
			mvo.setPnum(request.getParameter("jumin1")+"-"+request.getParameter("jumin2"));
			mvo.setEmail(request.getParameter("femail")+"@"+request.getParameter("bemail"));
			mvo.setPhone(request.getParameter("phone1")+"-"+request.getParameter("phone2")+"-"+request.getParameter("phone3"));
			mvo.setMyspot(request.getParameter("mySpot"));
			mvo.setMypark(request.getParameter("myPark"));
			
			//비지니스로직을 호출하여 DB에 가입정보를 insert한다.
			service.insertMember(mvo);
			
			//회원가입성공을 알리는 welcome.jsp를 뷰리졸버에서 찾아 페이지 이동
			return new ModelAndView("welcome","mvo",mvo);
		
		//회원의 정보를 업데이트
		case "update":
			
			//로그인 작업을 거치면서 세션에 회원정보를 넣어두었다.
			//그 회원이 정보를 수정하는 것이기 때문에 세션에 넣어둔 회원정보를 불러오고
			//바뀐 내용만 다시 셋팅
			mvo = (MemberVO)session.getAttribute("loginUser");
			mvo.setPass(request.getParameter("pass"));
			mvo.setEmail(request.getParameter("femail")+"@"+request.getParameter("bemail"));
			mvo.setPhone(request.getParameter("phone1")+"-"+request.getParameter("phone2")+"-"+request.getParameter("phone3"));
			//업데이트한 회원정보를 다시 세션에 싣고
			session.setAttribute("loginUser", mvo);
			//비지니스로직을 호출하여 db에 업데이트한다.
			service.updateMember(mvo);
			
			return new ModelAndView("resultUpdateMember");
			
		//회원탈퇴
		case "delete":
			
			//MemberVO에 회원정보수정페이지에서 넘어온 ID를 싣고 회원탈퇴로직을 호출한다. 
			mvo.setId(request.getParameter("id"));
			service.deleteMember(mvo);
			
			//현재 세션의 회원정보를 받아서
			mvo = (MemberVO)session.getAttribute("loginUser");
			
			//관리자 계정 "dacapo"가 아니라면 회원 자신이 스스로 탈퇴한 것이기 때문에 세션을 박탈
			if(!mvo.getId().equals("dacapo"))
				session.invalidate();
			
			//관리자 계정이 "dacapo"라면 관리자가 강제탈퇴 시킨 것이기 때문에 관리자의 세션을 죽일 필요가 없다.
			
			//뷰리졸버에서 해당.jsp파일을 찾아 정상탈퇴되었음을 알리는 뷰로 이동
			return new ModelAndView("resultDeleteMember");
		
		//전체회원정보열람
		case "getAll":
			//관리자로 로그인하면 사용 할 수 있는 관리페이지에서 요청이 들어온 것
			//서비스로직을 호출하여 전체회원정보리스트를 얻음
			List<MemberVO> mlist = (List<MemberVO>)service.getAllMembers();
			
			//전체회원정보리스트를 싣고 뷰리졸버에서 전체회원정보를 조회하는 뷰를 찾아 이동
			return new ModelAndView("allMemberList","memberList", mlist);
		
		//로그인
		case "login":
			
			//로그인이 되지 않는 이유를 담을 변수
			String msg = "";
			//로그인 폼에서 입력한 아이디를 받아 해당 아이디를 가진 정보가 있는지 비지니스 로직을 호출
			mvo = service.getMember(request.getParameter("id").trim());
			//리턴된 값이 없다면 아이디가 없다는 뜻이므로 msg변수에 해당 메세지 싣음
			if(mvo == null){
				msg = "ID가 존재하지 않습니다.";
			}else{
				//리턴된 값이 있다면 아이디가 존재한다는 뜻이므로 
				//입력한 password와 리턴된 객체의 password를 비교
				if(request.getParameter("pass").equals(mvo.getPass())){
					//두 값이 일치한다면 로그인 성공이므로 로그인유저의 정보를 담아 세션을 열어주고
					session.setAttribute("loginUser", mvo);
					//메인페이지로 이동시켜준다.
					return new ModelAndView("redirect:../index.jsp");
				}
				//두 값이 일치하지 않는다면 아이디는 존재하는데 비밀번호가 틀렸다는 메세지를 msg변수에 싣음.
				msg = "비밀번호번호를 다시 입력해주세요.";
			}
			//쿼리스트링으로 메세지를 담아 로그인전용페이지로 이동한다. 
			return new ModelAndView("../../index","msg",msg);
		
		//로그아웃
		case "logout":
			//로그아웃버튼을 눌러서 이동해온 것이므로 열려있는 세션을 닫아준다.
			session.invalidate();
			//메인페이지로 이동
			return new ModelAndView("redirect:../index.jsp");
		

		//이외의 요청
		default:
			//잘못된 기능요청임을 알리는 에러전달페이지로 이동
			return new ModelAndView("menuError");
		}	
	}
	
}
