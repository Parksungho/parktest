package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import vo.AirVO;
import vo.BikeRoadVO;
import vo.FestivalVO;

public class ParseDAOImpl extends SqlMapClientDaoSupport implements IParseDAO{

	@Override
	public void parseAirXml(AirVO aVo) {
		getSqlMapClientTemplate().insert("parseAir", aVo);
	}

	@Override
	public void ParseFestivalXml(FestivalVO fVo) {
		
		getSqlMapClientTemplate().insert("parseFestival", fVo);
	}

	@Override
	public void removeAirInfo() {
		getSqlMapClientTemplate().delete("removeAirInfo");
		
	}

	@Override
	public void removeFestivalInfo() {
		getSqlMapClientTemplate().delete("removeFestivalInfo");
		
	}


}
