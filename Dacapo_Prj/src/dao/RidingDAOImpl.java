package dao;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import vo.RidingBoardVO;
import vo.RidingListVO;
import vo.RidingReplyVO;

public class RidingDAOImpl extends SqlMapClientDaoSupport implements IRidingDAO
{
	public void RidingBoardInsert(RidingBoardVO rVo) 
	{
		getSqlMapClientTemplate().insert("insertRiding", rVo);
	}
	public void RidingBoardDelete(RidingBoardVO rVo) 
	{
		getSqlMapClientTemplate().delete("deleteRiding",rVo);
	}
	public RidingBoardVO RidingBoardGet(String id)
	{
		return (RidingBoardVO)getSqlMapClientTemplate().queryForObject("getRiding",id);
	}
	public List<RidingBoardVO> RidingBoardGetAll() 
	{
		return (List<RidingBoardVO>)getSqlMapClientTemplate().queryForList("getAllRiding");
	}
	public void RidingReplyInsert(RidingReplyVO rVo)
	{
		getSqlMapClientTemplate().insert("insertRidingReply", rVo);
	}
	public void RidingReplyDelete(RidingReplyVO rVo)
	{
		getSqlMapClientTemplate().delete("deleteRidingReply",rVo);
	}
	public void RidingReplyUpdate(RidingReplyVO rVo) 
	{
		getSqlMapClientTemplate().update("updateRidingReply",rVo);
	}
	public RidingReplyVO RidingReplyGet(String id) 
	{
		return (RidingReplyVO)getSqlMapClientTemplate().queryForObject("getRidingReply",id);
	}
	public List<RidingReplyVO> RidingReplyGetAll() 
	{
		return (List<RidingReplyVO>)getSqlMapClientTemplate().queryForList("getAllRidingReply");
	}
	public void RidingInsert(RidingListVO rVo) 
	{
		getSqlMapClientTemplate().insert("insertIntoRidingList", rVo);
	}
	public void RidingDelete(RidingListVO rVo) 
	{
		getSqlMapClientTemplate().delete("deleteFromRidingList", rVo);
	}
	public List<RidingListVO> RidingGetAll() 
	{
		return (List<RidingListVO>)getSqlMapClientTemplate().queryForList("getAllList");
	}
}
