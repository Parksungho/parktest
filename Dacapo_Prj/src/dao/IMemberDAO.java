package dao;

import java.util.List;

import vo.MemberVO;



public interface IMemberDAO {

	void insert(MemberVO mvo);
	void delete(MemberVO mvo);
	void update(MemberVO mvo);
	MemberVO get(String id);
	List<MemberVO> getAll();
	List<String> getSpots();
	List<String> getParks(String spot);
}
