package dao;

import java.util.List;

import vo.BoardVO;
import vo.ReplyVO;


public class BoardDAO {
	
	private IBoardDAO bdao;
	
	public void setBdao(IBoardDAO bdao) {
		this.bdao = bdao;
	}

	public void insertArticle(BoardVO bvo){
		bdao.insert(bvo);
	}
	
	public void deleteArticle(int num){
		bdao.delete(num);
	}
	
	public void updateArticle(BoardVO bvo){
		bdao.update(bvo);
	}
	
	public BoardVO getArticle(int num){
		return bdao.get(num);
	}
	
	public List<BoardVO> getAllArticles(){
		return bdao.getAll();
	}
	
	public void insertReply(ReplyVO rvo){
		bdao.insertReply(rvo);
	}
	
	public void deleteReply(int rnum){
		bdao.deleteReply(rnum);
	}
	
	public void updateReply(ReplyVO rvo){
		bdao.updateReply(rvo);
	}
	
	public ReplyVO getReply(int rnum){
		return bdao.getReply(rnum);
	}
	
	public List<ReplyVO> getAllReply(int bnum){
		return bdao.getAllReply(bnum);
	}
	
	public void deleteAllReply(int bnum){
		bdao.deleteAllReply(bnum);
	}
}
