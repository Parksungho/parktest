package dao;

import java.util.List;

import vo.MemberVO;

public class MemberDAO {
	private IMemberDAO mdao;
	
	public void setMdao(IMemberDAO mdao) {
		this.mdao = mdao;
	}

	public void insert(MemberVO mvo) {
		mdao.insert(mvo);
	}

	public void delete(MemberVO mvo) {
		mdao.delete(mvo);
	}


	public void update(MemberVO mvo) {
		mdao.update(mvo);
	}

	public MemberVO get(String id) {
		
		return mdao.get(id);
	}

	public List<MemberVO> getAll() {
		return mdao.getAll();
	}
	
	public List<String> getSpots() {
		return mdao.getSpots();
	}
	
	public List<String> getParks(String spot) {
		return mdao.getParks(spot);
	}
}
