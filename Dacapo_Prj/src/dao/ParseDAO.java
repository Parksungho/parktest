package dao;

import vo.AirVO;
import vo.BikeRoadVO;
import vo.FestivalVO;

public class ParseDAO {
	
	private IParseDAO pDao;
	
	public void setpDao(IParseDAO pDao) {
		this.pDao = pDao;
	}
	
	
	public void parseAirXml(AirVO aVo)
	{
		pDao.parseAirXml(aVo);
	}
	
	public void ParseFestivalXml(FestivalVO fVo)
	{
		pDao.ParseFestivalXml(fVo);
	}
	
	public void removeAirInfo(){
		pDao.removeAirInfo();
	}
	
	public void removeFestivalInfo(){
		pDao.removeFestivalInfo();
	}
}
