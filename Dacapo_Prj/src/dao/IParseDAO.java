package dao;

import vo.AirVO;
import vo.BikeRoadVO;
import vo.FestivalVO;

public interface IParseDAO 
{
	public void parseAirXml(AirVO aVo);
	public void ParseFestivalXml(FestivalVO fVo);
	public void removeAirInfo();
	public void removeFestivalInfo();
}
