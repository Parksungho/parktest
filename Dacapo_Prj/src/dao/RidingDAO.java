package dao;

import java.util.List;

import vo.RidingBoardVO;
import vo.RidingListVO;
import vo.RidingReplyVO;

public class RidingDAO 
{
	private IRidingDAO rdao;
	
	public void setRdao(IRidingDAO rdao)
	{
		this.rdao=rdao;
	}
	public void RidingBoardInsert(RidingBoardVO rVo)
	{
		rdao.RidingBoardInsert(rVo);
	}
	public void RidingBoardDelete(RidingBoardVO rVo)
	{
		rdao.RidingBoardDelete(rVo);
	}
	public RidingBoardVO RidingBoardGet(String id)
	{
		return rdao.RidingBoardGet(id);
	}
	public List<RidingBoardVO> RidingBoardGetAll()
	{
		return rdao.RidingBoardGetAll();
	}
	
	public void RidingReplyInsert(RidingReplyVO rVo)
	{
		rdao.RidingReplyInsert(rVo);
	}
	public void RidingReplyDelete(RidingReplyVO rVo)
	{
		rdao.RidingReplyDelete(rVo);
	}
	public void RidingReplyUpdate(RidingReplyVO rVo)
	{
		rdao.RidingReplyUpdate(rVo);
	}
	public RidingReplyVO RidingReplyGet(String id)
	{
		return rdao.RidingReplyGet(id);
	}
	public List<RidingReplyVO> RidingReplyGetAll()
	{
		return rdao.RidingReplyGetAll();
	}
	
	public void RidingInsert(RidingListVO rVo)
	{
		rdao.RidingInsert(rVo);
	}
	public void RidingDelete(RidingListVO rVo)
	{
		rdao.RidingDelete(rVo);
	}
	public List<RidingListVO> RidingGetAll()
	{
		return rdao.RidingGetAll();
	}
}
