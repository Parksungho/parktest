package dao;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import vo.FestivalVO;

public class FestivalDAOImpl extends SqlMapClientDaoSupport implements IFestivalDAO
{
	public List<FestivalVO> getAll() 
	{
		return (List<FestivalVO>)getSqlMapClientTemplate().queryForList("getAllFestival");
	}
}
