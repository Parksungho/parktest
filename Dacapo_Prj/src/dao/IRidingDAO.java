package dao;

import java.util.List;

import vo.RidingBoardVO;
import vo.RidingListVO;
import vo.RidingReplyVO;


public interface IRidingDAO
{
	public void RidingBoardInsert(RidingBoardVO rVo);
	public void RidingBoardDelete(RidingBoardVO rVo);
	public RidingBoardVO RidingBoardGet(String id);
	public List<RidingBoardVO> RidingBoardGetAll();
	
	public void RidingReplyInsert(RidingReplyVO rVo);
	public void RidingReplyDelete(RidingReplyVO rVo);
	public void RidingReplyUpdate(RidingReplyVO rVo);
	public RidingReplyVO RidingReplyGet(String id);
	public List<RidingReplyVO> RidingReplyGetAll();
	
	public void RidingInsert(RidingListVO rVo);
	public void RidingDelete(RidingListVO rVo);
	public List<RidingListVO> RidingGetAll();
}
