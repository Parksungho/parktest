package dao;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapClient;

import vo.MemberVO;

public class MemberDAOImpl extends SqlMapClientDaoSupport implements IMemberDAO{

	
	@Override
	public void insert(MemberVO mvo) {
		
		getSqlMapClientTemplate().insert("insertMember", mvo);
	}

	@Override
	public void delete(MemberVO mvo) {
		getSqlMapClientTemplate().delete("deleteMember", mvo);
		
	}

	@Override
	public void update(MemberVO mvo) {
		getSqlMapClientTemplate().update("updateMember", mvo);
		
	}

	@Override
	public MemberVO get(String id) {
		
		return (MemberVO)getSqlMapClientTemplate().queryForObject("getMember", id);
	}

	@Override
	public List<MemberVO> getAll() {
	
		return (List<MemberVO>)getSqlMapClientTemplate().queryForList("getAllMembers");
	}

	@Override
	public List<String> getSpots() {
		// TODO Auto-generated method stub
		return (List<String>)getSqlMapClientTemplate().queryForList("getSpots");
	}

	@Override
	public List<String> getParks(String spot) {
		// TODO Auto-generated method stub
		return (List<String>)getSqlMapClientTemplate().queryForList("getParks", spot);
	}
	

}
