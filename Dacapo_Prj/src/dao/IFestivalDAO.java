package dao;

import java.util.List;

import vo.FestivalVO;

public interface IFestivalDAO 
{
	public List<FestivalVO> getAll();
}
